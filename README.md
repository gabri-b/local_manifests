# Android for Raspberry Pi 4 (unfinished)

## Download Android source with patches (local_manifests)
 Refer to http://source.android.com/source/downloading.html
 ```
 $ repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r25
 $ git clone https://gitlab.com/rpi4_android/android/local_manifests.git .repo/local_manifests -b android-10
 $ repo sync
 ```

## Build for Raspberry Pi4
https://gitlab.com/rpi-droid/rpi4/tutorials-and-faqs

Use -j[n] option on sync & build steps, if build host has a good number of CPU cores.
